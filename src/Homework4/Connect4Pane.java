package Homework4;

import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Rectangle;
import javafx.scene.shape.Shape;

/**
 * fix gameboard
 * fix winning lights
 * add computer player?
 */
public class Connect4Pane extends Pane {
    private static final int ROWS=7, COLS=7;
    private static final double TILE_SIZE = 80;
    int i=0, j=0;
    Con4Game game = new Con4Game(ROWS, COLS);
    Shape connect;
    Label playerTurn, playerOneScore, playerTwoScore;
    Circle circle;

    /**
     * sets handler for when mouse is clicked
     */
    public Connect4Pane(){ this.setOnMouseClicked(eventHandler); }

    /**
     * paint method paints the board
     */
    public void paint() {
        getChildren().clear();

        //making gameboard connect
        connect = new Rectangle(0,50,(TILE_SIZE+10)*COLS, (ROWS+1)*TILE_SIZE);
        connect.setFill(Color.BLUE);
        getChildren().add(connect);

        //making label playerturn
        String playerLabel = ("Player "+(game.getCurrentPlayer()+1)+"'s turn!");
        playerTurn = new Label(playerLabel);
        String scoreOne = ("Player 1 Score: " + game.getGamesWon(0));
        String scoreTwo = ("Player 2 Score: " + game.getGamesWon(1));
        playerOneScore = new Label(scoreOne);
        playerTwoScore = new Label(scoreTwo);

        playerTurn.setAlignment(Pos.TOP_CENTER);
        playerTurn.setVisible(true);
        playerOneScore.setLayoutX(400);
        playerTwoScore.setLayoutX(500);
        getChildren().add(playerTurn);
        getChildren().add(playerOneScore);
        getChildren().add(playerTwoScore);

        //making circles on gameboard, adding player tokens
        for (i = 0; i < COLS; i++) {
            for (j = 0; j < ROWS; j++) {
                circle = new Circle(TILE_SIZE / 2);
                circle.setCenterX((i*TILE_SIZE)+TILE_SIZE /2+(TILE_SIZE/8));
                circle.setCenterY((ROWS-1-j)*(TILE_SIZE)+(50 + (TILE_SIZE / 2)+(TILE_SIZE/8)));
                if (game.getToken(j, i) == -1)
                    circle.setFill(Color.WHITE);
                else if (game.getToken(j, i) == 0) {
                    circle.setFill(Color.RED);
                } else {
                    circle.setFill(Color.YELLOW);
                }
                if(game.checkForWinner()==0&&game.getToken(j, i)==0){
                    circle.setFill(Color.GREEN);
                }
                if(game.checkForWinner()==1&&game.getToken(j, i)==1){
                    circle.setFill(Color.GREEN);
                }
                getChildren().add(circle);
            }
        }

        if(game.checkForWinner()==0||game.checkForWinner()==1){
            game.addWin(game.getCurrentPlayer());
            playerOneScore.setText("Player 1 Score: "+game.getGamesWon(0));
            playerTwoScore.setText("Player 2 Score: "+game.getGamesWon(1));
        }
        if(game.checkForWinner()==3){
            playerTurn.setText("Tie game!");
            playerOneScore.setText("Player 1 Score: "+game.getGamesWon(0));
            playerTwoScore.setText("Player 2 Score: "+game.getGamesWon(1));
        }
    }

    /**
     * determines which column has been selected
     */
    EventHandler<javafx.scene.input.MouseEvent>eventHandler = new EventHandler<MouseEvent>() {
        @Override
        public void handle(MouseEvent event) {
            int colX = (int) Math.floor((event.getX() / (TILE_SIZE + 5)));
            if(game.checkForWinner()==-1){
                game.makeMove(colX);
                paint();
            }
            if(game.checkForWinner()==0||game.checkForWinner()==1){
                playerTurn.setText("Player "+(game.getCurrentPlayer()+1)+" wins!");
            }
            if(game.checkForWinner()==-1){
            game.nextPlayer();}
        }
    };
}
