package Homework4;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.*;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Region;
import javafx.scene.paint.Color;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javafx.scene.Scene;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

/**
 * Emelie Gage Homework 4 - Connect 4
 * Answers to problems:
 *  1) A
 *  2) B
 *  3) C
 *  4) B
 *  5) C
 *  6) C
 *  7) A
 *  8) C
 *  9) D
 *  10) D
 *  11) A/C
 *  12) D
 *  13) B
 *  14) A, B, D
 *  */
public class Connect4Window extends Application {
    MenuItem helpItem, aboutItem, exitItem, saveItem, loadItem, clearItem, clearAllItem;
    Connect4Pane Pane = new Connect4Pane();
    int ROWS = 7, COLS = 7;
    int[][] gameArray;
    int currentPlayer, p1Score, p2Score;
    File FN;

    public static void main(String[] args) {
        launch(args);
    }

    /**
     * Set Scene and stage
     * @param primaryStage stage for our panes
     */
    @Override
    public void start(Stage primaryStage) {
        primaryStage.setTitle("Play Connect 4");
        final double scene_width = 605;
        final double scene_height = 700;
        MenuBar bar = new MenuBar();
        Menu fileMenu = buildFileMenu();
        Menu helpMenu = buildHelpMenu();
        bar.getMenus().add(fileMenu);
        bar.getMenus().add(helpMenu);

        BorderPane root = new BorderPane();
        root.setCenter(Pane);
        root.setTop(bar);

        Scene scene = new Scene(root, scene_width, scene_height);
        Pane.paint();
        primaryStage.setScene(scene);
        primaryStage.show();
    }

    /**
     * Builds help menu
     * @return help menu item
     */
    private Menu buildHelpMenu() {
        Menu helpM = new Menu("Help");
        helpItem = new MenuItem("Help: Connect Four");

        helpM.getItems().add(helpItem);

        HelpList listener = new HelpList();
        helpItem.setOnAction(listener);
        return helpM;
    }

    /**
     * Builds handler for help menu
     */
    private class HelpList implements EventHandler<ActionEvent> {
        @Override
        public void handle(javafx.event.ActionEvent e) {
            if (e.getSource() == helpItem) {
                helpMenu();
            }
        }
    }

    /**
     * Dialog to pop up when help menu is selected
     */
    private void helpMenu() {showInfoDialog("Click the column to drop down the token you want placed. If you get four of a color in a row, you win! Play with a friend!");
    }

    /**
     * Pops up message for either help or about
     * @param message message is passed through helpmenu/filemenu
     */
    private void showInfoDialog(String message){
        Alert a = (new Alert(Alert.AlertType.CONFIRMATION, message, ButtonType.FINISH));a.getDialogPane().setMinHeight(Region.USE_PREF_SIZE); a.show();
    }

    /**
     * Builds file menu
     * @return File menu item
     */
    private Menu buildFileMenu(){
        Menu fileM = new Menu("File");
        aboutItem = new MenuItem("About Connect Four");
        exitItem = new MenuItem("Exit");
        saveItem = new MenuItem("Save");
        loadItem = new MenuItem("Load");
        clearItem = new MenuItem("Clear Current Game");
        clearAllItem = new MenuItem("Clear All Scores");

        fileM.getItems().addAll(aboutItem, saveItem, loadItem, new SeparatorMenuItem(), clearItem, clearAllItem, exitItem);

        FileList listener = new FileList();
        saveItem.setOnAction(listener);
        loadItem.setOnAction(listener);
        aboutItem.setOnAction(listener);
        exitItem.setOnAction(listener);
        clearAllItem.setOnAction(listener);
        clearItem.setOnAction(listener);
        return fileM;
    }

    /**
     * Class to handle selection from the file menu
     */
    private class FileList implements EventHandler<javafx.event.ActionEvent> {
        @Override
        public void handle(javafx.event.ActionEvent e) {
            if (e.getSource() == aboutItem) {
                InfoMenu();
            }
            else if(e.getSource() == exitItem){
                System.exit(0);
            }
            else if(e.getSource() == saveItem){
                //Opens window for choosing file
                FileChooser fc = new FileChooser();
                fc.setInitialDirectory(new File("./"));
                File result = fc.showSaveDialog(null);
                if(result != null){
                    FN = result;
                    writeFile();
                }
            }
            else if(e.getSource() == loadItem){
                // Show a dialog to allow the user to choose files
                FileChooser of = new FileChooser();  //set starting point
                of.setInitialDirectory(new File("./"));
                File result = of.showOpenDialog(null);
                if (result == null){
                    showInfoDialog("You did not select a file.");
                }
                FN = result;
                Pane.game.set1GamesWon(p1File());
                Pane.game.set2GamesWon(p2File());
                Pane.game.setCurrentPlayer(turnFile());
                Pane.game.setGameArray(gameFile());
                Pane.paint();

            }
            if(e.getSource() ==clearItem){
                clearAllFields();
            }
            else if(e.getSource() == clearAllItem){
                showInfoDialog("Clicking this option has cleared all scores and board. Click finish to continue.");
                clearAllItems();
            }
        }
    }
    /**
     * this returns player turn from save file
     * @return current player
     */
    public int turnFile(){
        try{
            Scanner file = new Scanner(FN);
            currentPlayer  = file.nextInt();
            file.close();
        } catch (FileNotFoundException e) {
            System.out.println("File Not Found: " + e.getMessage());
            System.exit(0);
        }
        return currentPlayer;
    }

    /**
     * Next 2 return player 1 score and player 2 scores from saved file
     * @return p1File scores
     */
    public int p1File(){
        try{
            Scanner file = new Scanner(FN);
            file.next();
            p1Score = file.nextInt();
            file.close();
        } catch (FileNotFoundException e) {
            System.out.println("File Not Found: " + e.getMessage());
            System.exit(0);
        }
        return p1Score;
    }

    public int p2File(){
        try{
            Scanner file = new Scanner(FN);
            file.next();
            file.next();
            p2Score = file.nextInt();
            file.close();
        } catch (FileNotFoundException e) {
            System.out.println("File Not Found: " + e.getMessage());
            System.exit(0);
        }
        return p2Score;
    }

    /**
     * this returns game array from file
     * @return array
     */
    public int[][] gameFile(){
        try{
            gameArray = new int [ROWS][COLS];
            Scanner file = new Scanner(FN);
            file.next();
            file.next();
            file.next();
            for(int i=0; i<ROWS; i++){
                for(int j=0; j<COLS; j++){
                    gameArray[i][j]=file.nextInt();
                }
            }
            file.close();
        } catch (FileNotFoundException e) {
            System.out.println("File Not Found: " + e.getMessage());
            System.exit(0);
        }
        return gameArray;
    }

    /**
     * dialog for about menu to pop up on click
     */
    private void InfoMenu(){showInfoDialog("Connect Four Game\nAuthor: Sherri Harms & Emelie Gage\nEdition: 1.0\nDate 4/30/2019");
    }

    /**
     * This writes the current player and game array to file
     */
    public void writeFile(){
        try{
            FileWriter outFile = new FileWriter(FN);
            outFile.write(String.valueOf(Pane.game.getCurrentPlayer())+" ");
            outFile.write(String.valueOf(Pane.game.getGamesWon(0)+" "));
            outFile.write(String.valueOf(Pane.game.getGamesWon(1))+" \n");
            for(int i = 0; i<ROWS; i++){
                for(int j=0; j<COLS; j++){
                    outFile.write(String.valueOf(Pane.game.getToken(i, j))+" ");
                }
                outFile.write("\n");
            }
            // Close the file
            outFile.close();
        } catch (FileNotFoundException e) {
            System.out.println("File Not Found: " + e.getMessage());
            System.exit(0);
        } catch (IOException e) {
            System.out.println("IO Error: " + e.getMessage());
            System.exit(0);
        }
    }

    /**
     * Method to clear board and reset play
     */
    private void clearAllFields() {
        gameArray = new int[ROWS][COLS];
        for(int i=0; i<ROWS; i++){
            for(int j=0; j<COLS; j++){
                gameArray[i][j]=-1;
                if(Pane.game.getToken(i, j)==-1){
                    Pane.circle.setFill(Color.WHITE);
                }
            }
        }
        Pane.game.setGameArray(gameArray);
        Pane.game.setCurrentPlayer(0);
        Pane.paint();
    }

    /**
     * This method will clear scores as well as board
     */
    private void clearAllItems(){
        gameArray = new int[ROWS][COLS];
        for(int i=0; i<ROWS; i++){
            for(int j=0; j<COLS; j++){
                gameArray[i][j]=-1;
                if(Pane.game.getToken(i, j)==-1){
                    Pane.circle.setFill(Color.WHITE);
                }
            }
        }
        Pane.game.setGameArray(gameArray);
        Pane.game.set1GamesWon(0);
        Pane.game.set2GamesWon(0);
        Pane.game.setCurrentPlayer(0);
        Pane.paint();
    }
}