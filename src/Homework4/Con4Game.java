package Homework4;

import javafx.scene.paint.Color;

/**
 * Start of Solution for Connect 4 game implementation
 * Assignment 5 of CSIT 150
 *  @author Catherine Anderson
 * created Spring 2013
 * revised Spring 2014
 *
 */

public class Con4Game {

    private int rows;
    private int cols;
    private int[][] gameArray; // array that indicates which players tokens
    // are in which column
    private int currentPlayer;
    private int winner;
    private int player1wins;
    private int player2wins;

    Con4Game(int r, int c)
    {
        rows = r;
        cols = c;
        player1wins = 0;
        player2wins = 0;

        setGameArray(new int[rows][cols]);
        for(int row =0; row < rows; row++)
        {   for(int col =0; col < cols; col++)
        {   getGameArray()[row][col] = -1;  // -1 indicated no token present
        }
        }
        currentPlayer = 0;
    }

    /**
     * makes the move inserting token at current location
     * @param c current token
     */
    public void makeMove(int c)
    {
        for(int row = 0; row < rows; row++)
        {   if( getGameArray()[row][c] == -1)
        {getGameArray()[row][c] = currentPlayer;
            break;
        }
        }
    }

    /**
     * gets token at current location
     * @param r rows
     * @param c cols
     * @return game array with r & c passed
     */
    public int getToken(int r, int c) { return getGameArray()[r][c]; }

    /**
     * updates player turn
     */
    public void nextPlayer() {
        currentPlayer++;
        currentPlayer %= 2;
    }

    /**
     * @return the currentPlayer
     */
    public int getCurrentPlayer() {
        return currentPlayer;
    }

    /**
     * set current player to clear board
     * @param p =0
     */
    public void setCurrentPlayer(int p){currentPlayer = p;}

    /**
     * gets current array
     * @return updated game array
     */
    public int[][] getGameArray() {
        return gameArray;
    }

    /**
     * sets array
     * @param gameArray pass in array
     */
    public void setGameArray(int[][] gameArray) {
        this.gameArray = gameArray;
    }

    /**
     * Method to check for a winner in the gameboard
     * @return
     */
    public int checkForWinner(){
        winner = -1;
        for(int i=0; i<rows; i++) {
            for (int j = 0; j < cols - 3; j++) {
                if (getToken(i, j) == getToken(i, j+1) && getToken(i, j+1) == getToken(i, j+2)
                        && getToken(i, j+2) == getToken(i, j+3) &&getToken(i, j)!=-1){
                    winner = getCurrentPlayer();
                    }
            }
        }
        for(int i=0; i<rows-3; i++) {
            for (int j = 0; j < cols; j++) {
                if (getToken(i, j) == getToken(i+1, j) && getToken(i+1, j) == getToken(i+2, j)
                        && getToken(i+2, j) == getToken(i+3, j) &&getToken(i, j)!=-1) {
                    winner = getCurrentPlayer();
                }
            }
        }
        for(int i=0; i<rows-3; i++){
            for(int j=0; j<cols-3; j++){
                if(getToken(i, j)==getToken(i+1, j+1)&&getToken(i+1, j+1)==getToken(i+2,j+2)
                &&getToken(i+2,j+2)==getToken(i+3,j+3)&&getToken(i,j)!=-1) {
                    winner = getCurrentPlayer();
                }
            }
        }
        for(int i=3; i<rows; i++){
            for(int j=0; j<cols-3; j++){
                if(getToken(i, j)==getToken(i-1, j+1)&&getToken(i-1, j+1)==getToken(i-2, j+2)
                        &&getToken(i-2, j+2)==getToken(i-3, j+3)&&getToken(i, j)!=-1) {
                    winner = getCurrentPlayer();
                }
            }
        }
        if(getToken(6,0)!=-1&&getToken(6, 1)!=-1&&getToken(6, 2)!=-1&&getToken(6, 3)!=-1
                &&getToken(6, 4)!=-1&&getToken(6, 5)!=-1&&getToken(6, 6)!=-1){
            winner = 3;
        }
        return winner;
    }

    /**
     * This method adds win depending which player is passed in
     * @param p determines which player has the win
     */
    public void addWin(int p) {
        if(p==0)
            player1wins++;
        if(p==1)
            player2wins++;
    }

    /**
     * This method is used only to reset and load the scores
     * @param g for games
     */
    public void set1GamesWon(int g){
        player1wins=g;
    }
    public void set2GamesWon(int g){
        player2wins = g;
    }

    /**
     * This method gets games won for each player
     * @param p indicates which player has which score
     * @return different score per player
     */
    public int getGamesWon (int p) {
        if(p==0)
            return player1wins;
        else
            return player2wins;
    }
}